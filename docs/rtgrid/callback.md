# Установка базового колбека на поле #

*Доступные колбеки находятся в файлах rtgrid_column_callback и rtgrid_field_callback*

* $field - поле
* $default_callback - название метода класса колбека
* $extra_data - массив дополнительных данных для колбека. Не обязательный 

Пример использования
```
#!php

$crud->callback('active', 'active');
```
или
```
#!php

$crud->callback('name', 'rtgc_select', array(1 => 'name_1', 2 => 'name_2'));
```