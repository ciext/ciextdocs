# Вызывает колбек перед действием #
*Доступно только для таблицы*
Использование
```
#!php

$crud->before_action('delete', array($this->acl_model, 'before_delete_user'));

...

function before_delete_user($items = array()) {
    $q = $this->db->
            select('login')->
            where_in('id_acl_user', $items)->
            get('acl_user')->result_array();

    if (!empty($q)) {
        $this->acl_user_delete_observer->users = array_map(function($v) {
            return $v['login'];
        }, $q);
    }

    return TRUE;
}
```