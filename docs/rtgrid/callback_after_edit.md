# Вызывает колбек после insert/update данных #
*Доступно только для страницы редактирования*

Использование
```
#!php

$crud->callback_after_edit(array($this->acl_model, 'callback_after_edit_user'));

...

public function callback_after_edit_user($post_data, $pkey) {
    ...

    return TRUE;
}
```