# Добавляет мультиязычное поле #
*Примечание! В таблице с переводами должно быть поле primary_key с основной таблицы(в примере ниже это поле id_site_menu) и id_lan*

Входной параметр - объект класса multilang, описан в rtgrid/models/rtgrid_simple_types.php

```
#!php
$multi = new multilang(
   поле для отображения в гриде, 
   таблица переводов, 
   поле с таблицы переводов, 
   callback (если не задан использует callback по умолчанию)
)

```

Использование
```
#!php

...

$crud->multilang(new multilang('translate_name', 'site_menu_translate', 'name')); // callback не задан - отрисует поле по умолчанию
$crud->multilang(new multilang('translate_desc', 'site_menu_translate', 'desc', array($this->rtgrid_field_callback, 'texteditor')));

...

```