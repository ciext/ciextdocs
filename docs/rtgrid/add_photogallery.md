# Добавляет фото галерею #

Пример создания необходимых таблиц (добавить в инсталятор модуля)

```
#!php

// Create photogallery tables
$this->load->module('alphagallery/alphagallery_admin');
$config['main_table'] = 'portfolio_photo'; // Main photo table
$config['foreign_key'] = 'id_portfolio'; // foreign table primery key
$this->alphagallery_admin->create_tables($config);
```

Пример удаления таблиц (добавить в инсталятор модуля)
```
#!php

// Drop photogallery tables
$this->load->module('alphagallery/alphagallery_admin');
$config['main_table'] = 'portfolio_photo'; // Main photo table
$this->alphagallery_admin->drop_tables($config);
```

### Использование ###

**Обязательные параметры**

   * $table - название основной таблицы (string)
   * $photo_path - путь хранения изображения (string)

**Дополнительные возможности**

 * set_tab_label(string $label) - устанавливает заглавие на таб фотографий. 
 * set_photo_width(integer $photo_width) - автоматическая обрезка фото по ширине в $photo_width px
 * set_photo_height(integer $photo_height) - автоматическая обрезка фото по высоте в $photo_height px
 * set_translate_field(string $field, integer $length = 0) - добавление мультиязычного поля для галереи. **Важно - перезатирает поля по умолчанию. Поле должно присутствовать в таблице переводов**. $field - поле, $length - длина.*Пример: $photogallery->set_translate_field('name'); - в галереи будет только одно поле со стандартной длиной*

### Пример использования ###
```
#!php

$photogallery = new photogallery('portfolio_photo', '/assets/uploads/portfolio');
$crud->add_photogallery($photogallery);
```

### Как получить список фотографий? ###

```
// Используя библиотеку alphagallery_api
// Где main_table - таблица фотогалереи, foreign_key - поле для мепинга фотографий, $id_portfolio - ид фотогалереи (1,2, 15 ...) целое число
// Результатом работы функции будет массив обьектов photo_item. 
// Для получения пути к фото используем метод photo_item->get_photo(), 
//  photo_item->get_string('name') - имя фото, 
//  photo_item->get_lang_string('name', 2) - текст внесенный в админке на языке с ид 2
$photos = $this->load->library('alphagallery/alphagallery_api', array('main_table' => 'portfolio_photo', 'foreign_key' => 'id_portfolio'))->get_photos($id_portfolio);

// Или

$photogallery = new photogallery('portfolio_photo', '/assets/uploads/portfolio');
public function index() {
    $id = 1;

    $this->load->model('alphagallery/alphagallery_admin_model');
    $config['main_table'] = 'portfolio_photo';
    $config['foreign_key'] = 'id_portfolio';
    $this->alphagallery_admin_model->set_gallery_config($config);
    $this->alphagallery_admin_model->set_table($this->config->item('main_table'));
    $this->alphagallery_admin_model->set_pkey($this->config->item('alphagallery_photo_pkey'));
    $this->alphagallery_admin_model->set_lang_table($this->config->item('lang_table'));

    $content = array(
        'items' => array()
    );

    $items = $this->alphagallery_admin_model->fetch_by_fields(array($config['foreign_key'] => $id));
    foreach ($items as $item) {
        $content['items'][] = array(
            'src' => '/assets/uploads/portfolio/' . $item->get_photo(),
            'alt' => $item->get_string('name')
        );
    }

    return $this->parser->parse('portfolio/template', $content, TRUE);
}
```