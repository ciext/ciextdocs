Данная библиотека предоставляет интерфейс создания редактирования и удаления данных

### Пример использования ###
```
#!php
// Инициализация
$this->load->library('rtgrid/rtgrid_api');
$crud = & $this->rtgrid_api;
$crud->set_table('admin_menu');
$crud->set_primery_key('id_admin_menu');

$crud->columns('name, page, parent_id, view_order, active');
$crud->fields('name, page, parent_id, view_order, active');

$crud->display_as('name', $this->lang->line('crud_menu'));
$crud->display_as('page', $this->lang->line('crud_page'));

$crud->callback('active', 'active');

$crud->callback_column('page', array($this, 'callback_view_page'));
$crud->callback_column('parent_id', array($this, 'callback_view_parent_id'));

$crud->callback_field('page', array($this, 'callback_edit_page'));
$crud->callback_field('parent_id', array($this, 'callback_edit_parent_id'));

echo $crud->render();
```

# Методы

* **[set_table(string $table)](set_table)** - установка главной таблицы
* **[set_primery_key(string $pkey)](set_primery_key)** - установка первичного ключа
* **[set_order_by(string $column, string $direction)](set_order_by)** - установка сортировки в таблице по умолчанию
* **[columns(string $column)](columns)** - установка колонок таблицы
* **[fields(string $fields)](fields)** - установка полей при редактировании
* **[display_as(string $field, string $text)](display_as)** - установка перевода поля
* **[callback(string $field, string $default_callback, array $extra_data)](callback)** - установка базового колбека на колонку таблицы и поля
* **[callback_column(string $column, callback $callback, array $extra_data)](callback_column)** - установка колбека на колонку таблицы
* **[callback_field(string $field, callback $callback, array $extra_data)](callback_field)** - установка колбека на поле
* **[add_top_action(object $action)](add_top_action)** - добавление кнопки общих действий в таблицу
* **[add_action(object $action)](add_action)** - добавление кнопки действий в строку таблицу
* **[unset_top_action(string $action)](unset_top_action)** - удаление кнопки общих действий с таблицы
* **[unset_action(string $action)](unset_action)** - удаление кнопки действий с таблицы или редактирования
* **[before_action(string $action, callback $callback)](before_action)** - вызывает колбек перед действием
* **[callback_before_edit(callback $callback)](callback_before_edit)** - вызывает колбек перед insert/update данных
* **[callback_after_edit(callback $callback)](callback_after_edit)** - вызывает колбек после insert/update данных
* **[attach(SplObserver $obs)](attach)** - добавляет обсервер
* **[detach(SplObserver $obs)](detach)** - удаляет обсервер
* **[where(string $key, string $value, boolean $escape)](where)** - аналогично $this->db->where();
* **[where_in(string $key, string $value, boolean $escape)](where_in)** - аналогично $this->db->where_in();
* **[join(string $table, string $cond, string $type)](join)** - аналогично $this->db->join();
* **[multilang(multilang $multilang)](multilang)** - добавляет мультиязычное поле
* **[set_image_upload(object $image_upload)](set_image_upload)** - устанавливает картинку для загрузки с возможностью обрезки и изменения размеров
* **[set_relation(string $field_name, string $related_table, string $related_title_field, mixed $where, string $order_by)](set_relation)** - устанавливает отношение 1:n
* **[set_relation_n_n(string $field_name, string $mapping_table, string $selected_table, string $selected_table_alias_field)](set_relation_n_n)** - устанавливает отношение n:n
* **[readonly(string $field)](readonly)** - устанавливает поле только для чтения
* **[hidden(string $field, mixed $value)](rhidden)** - устанавливает скрытое поле
* **[required_fields(string $field, string $field1 ...)](required_fields)** - устанавливает обязательное поле
* **[add_photogallery(photogallery $photogallery)](add_photogallery)** - добавляет фотогалерею
* **[render()](render)** - отрисовка HTML
* **[multifield(string $field)](multifield)** - добавляет мультиполя
* **[unique_fields(string $field)](unique_fields)** - проверка поля на уникальность при сохранении

