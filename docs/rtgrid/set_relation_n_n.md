# set_relation_n_n #
Устанавливает соотношение n:n
**set_relation_n_n(string $field_name, string $mapping_table, string $selected_table, string $selected_table_alias_field, array $selected_table_where)**

* $field_name - поле для отображения при редактировании
* $mapping_table - таблица для сохранения данных, сохраняются значение первичных ключей с главной и $selected_table таблиц
* $selected_table - таблица с которой достаются все доступные значения для меппинга
* $selected_table_alias_field - поле псевдонима с таблицы $selected_table
* $selected_table_where - массив условий для выборки с таблицы $selected_table. Ex. array('active' => true)

### Создание таблицы ###

Перед использование метода нужно создать таблицу соответствий. Пример таблицы

```
CREATE TABLE IF NOT EXISTS `am_search_key_linker` (
  `id_am_search_key_linker` int(11) NOT NULL AUTO_INCREMENT,
  `id_am_search` int(11) NOT NULL,
  `id_am_search_keys` int(11) NOT NULL,
PRIMARY KEY (`id_am_search_key_linker`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
```

### Пример использования ###

```
#!php
$this->load->library('rtgrid/rtgrid_api');

$crud = & $this->rtgrid_api;
$crud->set_table('am_search');
$crud->set_primery_key('search_id');
        
$crud->columns('name, description, amount_max, actual_amount, active');
$crud->fields('name, description, amount_max, actual_amount, keys, active');
        
$crud->set_relation_n_n('keys', 'am_search_key_linker', 'am_search_keys', 'key_name');

$crud->render();
```