# Добавляет обсервер #

### Пример добавления обсервера при помощи хелпера ###

1. Загружаем хелпер - $this->load->helper('rtgrid/rtgrid_obs');
2. Добавляем обсервер - $crud->attach(make_rtgrid_delete_obs('acl ресурс', 'acl действие', 'сообщение', 'доп. информация')); 

где

* 'acl ресурс' - идентификатор модуля в системе. Например - blog
* 'acl действие' - действие, сообщение обсервера смогут видеть только пользователи которым доступно данное действие
* 'сообщение' - сообщение, в данном сообщении переменная {link} изменится на линк записи, а {module} - дополнительную информацию см. ниже. Пример - "Добавил новую <a href="{link}">запись</a>{module}".  *Необязательное*
* 'доп. информация' - дополнительная информация, переменная %s заменится на название модуля с файла install_lang текущего модуля. Пример - "<br />Модуль - "%s"".  *Необязательное*


```
#!php
// Загружаем хелпера
$this->load->helper('rtgrid/rtgrid_obs');

// Настраиваем ACL и добавляем объект обсервера
$crud->attach(make_rtgrid_add_obs('acl', 'edit_user'));
$crud->attach(make_rtgrid_edit_obs('acl', 'edit_user'));
$crud->attach(make_rtgrid_delete_obs('acl', 'edit_user'));
```
### Пример добавление собственного обсервера ###

```
#!php
// Загружаем обсервер
$this->load->library('ciext/observers/acl_user/acl_user_delete_observer');

// Добавляем объект обсервера
$crud->attach($this->acl_user_delete_observer);

...

// Класс обсервера
class acl_user_delete_observer implements SplObserver {
    
    public $users = array();

    public function update(SplSubject $subject) {
        if ($subject->current_action == 'delete' && $subject->current_action_return) {
            $CI = & get_instance();
            $CI->load->helper('ciext/observer');
            $CI->lang->load('ciext/observers/acl_user');
            $save = new saveObserver();
            $save->set_access('acl', 'edit_user');
            $save->set_text(sprintf($CI->lang->line('Delete users'), implode('", "', $this->users)));

            $save->save();
        }
    }

}
```