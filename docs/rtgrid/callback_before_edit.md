# Вызывает колбек перед insert/update данных #
*Доступно только для страницы редактирования*

Использование
```
#!php

$crud->callback_before_edit(array($this->acl_model, 'callback_before_edit_user'));

...

public function callback_before_edit_user($post_data, $pkey) {
    $this->load->library('RT_user');
    if (isset($post_data['password'])) {
        $post_data['password'] = $this->rt_user->password_hash($post_data['password']);
    }

    $post_data['birth_date'] = date('Y-m-d', strtotime($post_data['birth_date']));
    $post_data['registered'] = date('Y-m-d', strtotime($post_data['registered']));

    // Disable deactivate current user
    if ($pkey == $this->rt_user->id_user) {
        $post_data['active'] = '1';
    }

    return $post_data;
}
```