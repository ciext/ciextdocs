# Добавление кнопки общих действий в таблицу #

*Принимает объект класса rtgrid_top_action описание класса в файле rtgrid_simple_types*

Пример создания объекта класса rtgrid_top_action
```
#!php

$top_action = new rtgrid_top_action(
    идентификатор события, 
    надпись на кнопке, 
    колбек, 
    сообщение для подтверждения
);
```
Если в объекте не задан колбек, параметр $text будет выступать в роли шаблона. Нужно для создания кнопок перехода.
```
#!php

$crud->add_top_action(
    new rtgrid_top_action(
        'add', 
        '<a href="/' . $this->uri->uri_string() . '/add" class="btn"><i class="icon-plus"></i> ' . $this-lang->line('btn_add_text') . '</a>'
));
```
Использование с колбеком
```
#!php

$crud->add_top_action(
    new rtgrid_top_action(
        'delete', 
        '<i class="icon-trash"></i> ' . $this->lang->line('btn_delete_text'), 
        array($this->rtgrid_action_model, 'delete'), 
        $this->lang->line('msg_delete_data')
));
```