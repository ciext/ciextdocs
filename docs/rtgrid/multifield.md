# Установка мультиполей #

**multifield(strinf $field);**

   **Особенность**
  
   * при использовании на на фронте необходимо сделать json_decode($field); 

### Пример использования ###

```
#!php
$this->load->library('rtgrid/rtgrid_api');

$this->rtgrid_api->set_table('contact');
$this->rtgrid_api->columns('phone, skype, view_order, active');
$this->rtgrid_api->fields('phone, skype, view_order, active');
$this->rtgrid_api->multifield('phone');

$crud->render();
```