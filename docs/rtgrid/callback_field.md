# Установка колбека на поле #

* $field - поле
* $callback - колбек, любые форматы доступные php

### Пример использования ###

```
#!php

$crud->callback_field('page', array($this, 'callback_edit_page'));

...

function callback_edit_page($name, $value) {
    return '<input type="text" name="' . $name . '" value="' . $value . '" />';
}
```