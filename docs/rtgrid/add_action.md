# Добавление кнопки действий в строку таблицы или в редактирование записи #

*Принимает объект класса rtgrid_action или rtgrid_edit_action, описание класса в файле rtgrid_simple_types*

Пример создания объекта класса rtgrid_action/rtgrid_edit_action
```
#!php
// rtgrid_action
$top_action = new rtgrid_action(
    идентификатор события, 
    надпись на кнопке, 
    колбек, 
    сообщение для подтверждения
);

// rtgrid_edit_action
$top_action = new rtgrid_edit_action(
    идентификатор события, 
    надпись на кнопке, 
    колбек, 
    сообщение для подтверждения
);
```
Если в объекте не задан колбек, параметр $text будет выступать в роли шаблона. Нужно для создания кнопок перехода.

*Доступно только для класса rtgrid_action*
```
#!php

$crud->add_action(
    new rtgrid_action(
        'edit', 
        '<a href="/' . $this->uri->uri_string() . '/edit/{primery_key}"><i class="icon-pencil"></i> ' . $this->lang->line('btn_edit_text') . '</a>'
));

```
Использование с колбеком
```
#!php

$crud->add_action(
    new rtgrid_action(
        'delete', 
        '<i class="icon-trash"></i> ' . $this->lang->line('btn_delete_text'), 
        array($this->rtgrid_action_model, 'delete'), 
        $this->lang->line('msg_delete_data')
));
```
Пример колбека
```
#!php

    public function delete($ids) {
        foreach ($ids as $id) {
            foreach ($this->where as $where) {
                $this->db->where($where->key, $where->value, $where->escape);
            }
            
            $this->db->
                    where($this->table . '.' . $this->primery_key, $id)->
                    delete($this->table);
        }
        
        return ($this->db->affected_rows() > 0);
    }
```