# Устанавливает соотношение 1:n #

**set_relation(string $field_name, string $related_table, string $related_title_field, mixed $where, string $order_by)**

* $field_name - поле в родительской таблице
* $related_table - зависимая таблица (с которой нужно достать данные)
* $related_title_field - поле в зависимой таблице, текст с данного поля будет отображаться в круде
* $where - стандартное where фреймворка. *Опциональный параметр*
* $order_by - сортировка. Доступно значение ASC и DESC. *Опциональный параметр*

### Пример использования ###

```
#!php
$this->load->library('rtgrid/rtgrid_api');

$crud = & $this->rtgrid_api;
$crud->set_table('brief_item');
$crud->set_primery_key('id_brief_item');
        
$crud->columns('id_brief_category, brief_item.name, id_brief_item_type, brief_item.price, brief_item.active');
$crud->fields('name, id_brief_category, id_brief_item_type, price, active');
        
$crud->set_relation('id_brief_category', 'brief_category', 'name');
$crud->set_relation('id_brief_item_type', 'brief_item_type', 'name');

$crud->render();
```