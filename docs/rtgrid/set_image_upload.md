# Установка изображения для загрузки с возможностью кропа и изменения размеров #

**set_image_upload(new image_upload());**

   **Обязательные параметры**

   * 'image' - название поля в котором будет хранится изображение (string)
   * 'path' - путь хранения изображения (string). По умолчанию, это '/assets/uploads/multimedia/images'

   **Необязательные параметры**

   * 'width' - ширина изображения (int)
   * 'height' - высота изображения (int)
   * 'crop' - нужен ли кроп (boolean)
   * 'auto_open' - возможность авто открытия диалога с кропом (boolean)
   * 'lib' - библиотека которая используется для обработки изображения (string) - по умолчанию ('image_change'), доступное значение так же 'image_lib'
   * 'resize_after_crop' - нужно ли автоматическое изменение размеров изображения после кропа, до размеров 'width' и 'height' (boolean)
   * 'original' - нужно ли сохранять оригинальное изображение (boolean)
   * 'original_folder' - путь хранения оригинального изображения (string). По умолчанию, это '/assets/uploads/multimedia/images/original' (то есть это  'path' + '/original')

### Пример использования ###

```
#!php
$this->load->library('rtgrid/rtgrid_api');

$this->rtgrid_api->set_table('slider');
$this->rtgrid_api->columns('image, title, link, view_order, active');
$this->rtgrid_api->fields('image, title, link, view_order, active');
$this->rtgrid_api->set_image_upload(new image_upload('image', '/assets/uploads/images/thumbs/', 1440, 409, TRUE, TRUE, 'image_lib', TRUE, TRUE, '/assets/uploads/images/thumbs/original/'));

$crud->render();
```