# Установка колонок таблицы #

*Список полей, которые будут отображаться в таблице, допустимо использование названий таблиц и псевдонимов*

Пример использования
```
#!php

$crud->columns('name, page, parent_id, view_order, active');
```

Пример использования с названием таблицы
```
#!php

$crud->columns('admin_menu.name, admin_menu.page, admin_menu.parent_id, admin_menu.view_order, admin_menu.active');
```

Пример использования с псевдонимами
```
#!php

$crud->columns('am.name, am.page, am.parent_id, am.view_order, am.active');
```