# Установка колбека на колонку таблицы #

* $column - колонка
* $callback - колбек, любые форматы доступные php

### Пример использования ###

```
#!php

$crud->callback_column('page', array($this, 'callback_view_page'));

...

function callback_view_page($value, $data) {
// $data - содержит все данные записи.
    return base_url() . $value;
}
```
