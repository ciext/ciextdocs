# Добавление собственных переменных в конфигуратор системы #

Для добавления в системные настройки своего ключа, нуно создать в модуле в каталоге config файл bootstrap.php, который содержит ключ system_configurator

Пример файла
```
// myModule/config/bootstrap.php

return array(
    'system_configurator' => array(
        'manager_email'   => array(            // ключ для конфигуратора
            'label'    => 'Manager email',     // Перевод для ключа. Система подставит $this->lang->line('Manager email')
            'callback' => 'rtgc_select',       // Callback
            'callbck_options' => array(1 => 'ivan@mail.com'),
        ),
    ),
);
```

### Доступные колбеки ###

Доступны колбеки 3 типов:

1. стандартный PHP - Пример: 'callback' => function($n, $v){return $n . $v;} или 'callback' => array($obj, 'foo')
2. библиотеки колбеков rtgrid-a в каталоге ...rtgrid/libraries/callback. Если в названии колбека префикс rtgc_. Пример: 'callback' => 'rtgc_select'
3. методы класса configurator_callbacks. Пример: 'callback' => 'input', 'callback' => 'textarea', 'callback' => 'radio'

### Получение значения ключа ###

Для получения значения используется библиотека конфигуратора
```
$this->load->library('ciext/configurator');
$cnf = new configurator('system');
echo $cnf->item('manager_email');

```
