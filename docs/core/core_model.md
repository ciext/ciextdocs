Модель ядра

# Пример использования #

```
$this->load->model('core_model');
// Добавляем seo данные текущей страницы
$this->core_model->set_seo_data(array(
    'title' => 'SEOtitle',
    'keywords' => 'SEOkeywords',
    'description' => 'SEOdescription'
));
```
### set_seo_data(array $data)
Добавляет seo данные текущей страницы
```
$this->core_model->set_seo_data(array(
    'title' => 'SEOtitle',
    'keywords' => 'SEOkeywords',
    'description' => 'SEOdescription'
));
```
### create_page_url(string $page = '', integer $parent_id = 0)
Строит url страницы учитывая вложеность и язык
```
echo $this->core_model->create_page_url('dealers');
```