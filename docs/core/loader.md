Расширенный загрузчик codeigniter

### allBootstrap
Загружает файлы bootstrap.php всех модулей с папки config в глобальный конфиг
```
// Вызов
$this->load->allBootstrap();
// пример получения элемента
var_dump($this->config->item('search_module'));
```

Пример файла bootstrap.php
```
<?php

return array(
    'search_module' => array(
        'libraries' => array(
            'some data',
            'some data 2',
        ),
    ),
);
```