Данная библиотека позволяет изменять изображения на лету

# Пример использования #

```
$this->load->library('image');
// Обрезаем фото test.jpg по размерам: ширина 100, высота 50
echo $this->image->getImage('test.jpg' , 100, 50);
```

# Все свойства класса можно переопределить передав на конструктор библиотеки или вызвав соответствующие методы #

```
$this->load->library('image', array(
    'sourcePath'         => 'assets/uploads/multimedia/images', // базовый путь к исходным фото
    'croppedPath'        => 'assets/uploads/cropped',           // базовый путь для обрезанных фото
    'proccessLibrary'    => 'image_change',                     // библиотека для обрезания фото, доступные значения image_change и image_lib
    'watermark'          => array(...),                         // массыв для настройки вотремарка, используется стандартный функционал библиотеки image_lib фреймворка
));
echo $this->image->getImage('full or relative image path' , 100, 50);
```


### getImage(string $fullImagePath, integer $width, integer $height)

* $fullImagePath - полный или относительный путь к исходной фотографии
* $width - ширина
* $height - высота

Возвращает путь к обрезанной фотографии или путь к исходной фотографии в случаи неудачи
```
// Обрезаем фото по размерам: ширина 100, высота 50
echo $this->image->getImage('product/test.jpg' , 100, 50);
```
