# Описание

Данная библиотека предназначена для получения информации о языках с базы данных платформы

Пример использования

```
#!php

$this->load->library('db_lang_lib');
$id_lan = $this->db_lang_lib->get_current_lang_id(); // Получить ИД текущего языка

```
### get_current_lang_id() ###
Получение ИД текущего языка.
Возвращает integer
```
#!php
$id_lan = $this->db_lang_lib->get_current_lang_id();

```

### get_lang_by_id(integer $id_lan) ###
Получение данные активного языка по ИД.
Возвращает обьект или массив обьектов (если передать ид языка 0)

```
#!php
$lan = $this->db_lang_lib->get_lang_by_id(1);
echo $lan->id_lan;
echo $lan->short_name;
echo $lan->name;

```
