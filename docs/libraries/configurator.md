# Описание

Данная библиотека предназначена для простой манипуляции данными.

*Примечание! Можно использовать только при наличии acl ресурса в проекте*

Пример использования

```
#!php

$this->load->library('ciext/configurator');
$conf = new configurator('acl_test_module');  // Создаем объект для удобства и устанавливаем ресурс 'acl_test_module'
$conf->add_input('test key'); // добавляем элемент с ключем 'test key', отображается в виде input

return $conf->render();  // Вывод на экран
```
### set_resource ###
Установка acl ресурса - set_resource(string $acl_resource), $acl_resource - название ресурса модуля при инсталляции
*Примечание! Ресурс можно передавать на конструктор класса - new configurator($acl_resource)*

```
#!php
$conf->set_resource('acl_test_module');

```
### add_input ###
Добавление элемента input - add_input(string $key), $key - ключ по которому будет доступно значение

```
#!php

$conf->add_input('test key');
```

### add_textarea ###
Добавление элемента textarea - add_textarea(string $key), $key - ключ по которому будет доступно значение

```
#!php

$conf->add_textarea('test key');
```

### add_radio ###
Добавление элемента radio - add_radio(string $key), $key - ключ по которому будет доступно значение

```
#!php

$conf->add_radio('test key');
```

### add_item ###
Установка собственного элемента отображения - add_item(string $key, array $callback), $key - ключ по которому будет доступно значение, $callback - array(object, function)

```
#!php

public function conf() {
  ...
  $conf->add_item('test', array($this, 'test'));

  return $conf->render();
}

public function test($key, $value) {

  return '$key = ' . $key . ' $value = ' . $value;
}

Или 

public function conf() {
  ...
  $conf->add_item('test', function($key, $value){return $key.$value;});

  return $conf->render();
}
```

### display_as ###
Установка псевдонима на ключ - display_as(string $key, string $value);

```
#!php

$conf->display_as('test', 'Allias for test key');
```

### get_config ###
Получить все данные - array get_config()

```
#!php

$data = $conf->get_config();
echo $data['test'];
```

### item ###
Получить значение по ключу - string item(string $key)

```
#!php

echo $conf->item('test');
```

### set_config ###
Ручное сохранение данных - boolean set_config(array $data);

```
#!php

$data = array('test' => 'test value);
$conf->set_config($data);
```