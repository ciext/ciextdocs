Данная библиотека позволяет вставлять JavaScript и CSS файлы на страницы

# Пример использования #

```
$this->load->library('js_css');
// Добавляем js файл
$this->js_css->add_js('/assets/js/jquery.js');
```

### add_js
Add js file to js list
```
$this->js_css->add_js('/assets/js/jquery.js');
```

### add_css
Add css file to css list
```
$this->js_css->add_css('/assets/css/jquery.css');
```

### add_js_string
Add js string to js_string_list. This text will be add to page head.
```
$this->js_css->add_js_string('alert(\'hello world\');');
```

### add_css_string
Add css text to css_string_list. This text will be add to page head.
```
$this->js_css->add_css_string('#table{color:red;}');
```
### parse_js
Add js file from module to project head - parse_js(string $file, array $content)
Ex. if js file name is test.js and it located in myModule/views/js
    then line be like this - $this->js_css->parse_js('myModule/js/test.js');
```
$this->js_css->parse_js('myModule/js/test.js', array('item' => '1'))
```

### parse_css
Add css file from module to project head parse_css(string $file, array $content)
Ex. if css file name is test.css and it located in myModule/views/css
    then line be like this - $this->js_css->parse_css('myModule/css/test.css');
```
$this->js_css->parse_css('myModule/css/test.css', array('item' => '1'))
```

### remove_js
Remove js file
```
$this->js_css->remove_js('myModule/js/test.js');
```

### remove_css
Remove css file
```
$this->js_css->remove_css('myModule/css/test.css');
```

### js_reverse
Reverses the order of loading files - js_reverse(string $first_file, string $second_file)
```
$this->js_css->js_reverse('main.js', 'jquery.js');
```

### css_reverse
Reverses the order of loading files - css_reverse(string $first_file, string $second_file)
```
$this->js_css->css_reverse('main.css', 'jquery.css');
```

### get_js_string
Create js string from js_list

### get_css_string
Create css string from css_list